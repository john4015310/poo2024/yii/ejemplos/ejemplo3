<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Emple $model */

$this->title = $model->emp_no;
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="emple-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'emp_no' => $model->emp_no], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'emp_no' => $model->emp_no], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'emp_no',
            'apellido',
            'oficio',
            'dir',
            'fecha_alt',
            'salario',
            'comision',
            'dept_no',
            //opcion 1
            //'deptNo.dnombre',
            //opcion 2
            [
                'label' => 'Departamentox',
                'value' => $model->deptNo->dnombre
            ]
        ],
    ]) ?>

</div>
